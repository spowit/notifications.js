var _             = require('lodash'),
    should        = require('should'),
    kue           = require('kue'),
    notifications = require('../index');
    assert        = require('assert');
    notificationsConfig = require('../localConfig').config;


describe('Notifications', function(){

  var queuedJobs = notifications.delivery().queuedJobs();
  it('get queuedJobs', function() {
    assert.equal(!!queuedJobs, true, "queue not loaded");
  });

   describe('Email Service', function(){
     it('should configure correctly', function(){
       notifications.configure({
         withKueServer: true,
         services: {
           email: notificationsConfig.services.email
         },

         admin: notificationsConfig.admin
       });
     });

     it('should send emails', function(done){
       var users = [
       //{
       // name: 'Gabriel Cordero',
       // firstName: "Gabriel",
       // email: 'edward1@moblox.io'
       //},
       {
         name: 'Edward Alvarado',
         firstName: "Edward",
         email: 'edward2@moblox.io'
       }];

       var contentTemplate = {
         title: 'welcome email for {{name}}', // I'm not seeing this anywhere...
         to: '{{email}}',
         from: 'test@test.com',
         subject: 'Testing...',

         templateName: 'email_template',

         content: {
           title: "Hi {{name}}!! This is test. This should be visible inside the email",
           message: "This is the body content.",
           action_link: "http://www.google.com",
           action: "This button goes to google.com"
         }
       };

       var remainingJobs = users.length;

       try {
         notifications.sendEmail(users, contentTemplate);
       } catch (err) {
         done(err);
       }

       queuedJobs.on('job complete', function(id,result){
         if (--remainingJobs == 0) { done(); }
       })

     })
   });

  //describe('Android Service', function(){

    //it('should configure correctly', function(){
      //notifications.configure({
        //withKueServer: true,

        //services: {
          //android: notificationsConfig.services.android
        //},

        //admin: {
          //apiEmail: 'notifications@moblox.io',
          //adminEmail: 'mario@moblox.io',
          //templateName: 'admin'
        //}
      //});
    //})

    //it('should send push notifications', function(done){
      //var notificationMessage = 'test from notifications.js';

      //var users = [{
        //name: 'Mario Garces',
        //email: 'mario@moblox.io',
        //devices: {
          //android: [{
            //deviceId: 'APA91bE5U2tNmUlxfwBvYmZlwI2GpUVyAt0YuYN-2r5ZmZDfnNhR2Fbd6B0fxX7mOSaW1LakqZ5vBGxAmlXe5mCngcSTpHa4Q0ilxBe7HycOyqxsIMp_waxSj-mRA92iaVpiawuXUqrQHAA1v_T9IxNKvb1Wrwr3QQ',
            //isActive: true
          //}]
        //}
      //}];

      //var remainingJobs = users.length;

      //try {
        //notifications.sendPushNotifications(users, notificationMessage, {})
      //} catch (err) {
        //done(err);
      //}

      //queuedJobs.on('job complete', function(id,result){
        //if (--remainingJobs == 0) { done(); }
      //})
    //})


  //})

  // describe('iOS Service', function(){
  //   it('should configure correctly', function(){
  //
  //     notifications.configure({
  //       withKueServer: true,
  //
  //       services: {
  //         ios: notificationsConfig.services.ios
  //       },
  //
  //       admin: {
  //         apiEmail: 'notifications@moblox.io',
  //         adminEmail: 'mario@moblox.io',
  //         templateName: 'admin'
  //       }
  //     });
  //   })
  //
  //   it('should send push notifications', function(done){
  //     var notificationMessage = 'test from notifications.js';
  //
  //     var users = [{
  //       name: 'Mario Garces',
  //       email: 'mario@moblox.io',
  //       devices: {
  //         ios: [{
  //           deviceId: 'da8b7200a224eff79909ac3f3dba82699d03b11c6f25ba193ead996ec53c9e1a',
  //           isActive: true
  //         }]
  //       }
  //     }, {
  //       name: 'Edward Alvarado',
  //       email: 'mario@moblox.io',
  //       devices: {
  //         ios: [{
  //           deviceId: 'da8b7200a224eff79909ac3f3dba82699d03b11c6f25ba193ead996ec53c9e1a',
  //           isActive: true
  //         }]
  //       }
  //     }];
  //
  //     var remainingJobs = users.length;
  //
  //     try {
  //       notifications.sendPushNotifications(users, notificationMessage, {});
  //     } catch (err) {
  //       done(err);
  //     }
  //
  //     queuedJobs.on('job complete', function(id,result){
  //       if (--remainingJobs == 0) { done(); }
  //     })
  //   })
  // })


});
