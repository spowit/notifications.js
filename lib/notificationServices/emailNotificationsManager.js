var colors         = require('colors');
var path           = require('path');
var EmailTemplate = require('email-templates').EmailTemplate;
var nodemailer     = require('nodemailer');
    

function EmailNotificationManager(config){
  this.isConfigurated = false;
  this.templatesDir = path.join(__dirname, 'public', 'templates');
  this.mailer = null;
}

EmailNotificationManager.prototype.configure = function(config){
  this.isConfigurated = true;
  this.templatesDir = config.templatesDir;
  this.mailer = nodemailer.createTransport("SMTP", config.smtp);
};

EmailNotificationManager.prototype.sendEmail = function(emailData, options, callback){
  var self = this;
  
  options = options || {};
  
  if (!self.isConfigurated) {
    console.log('[Notifications.js]'.cyan, 'EmailNotifications: '.magenta, '[Warning: EmailNotifications is not configurated (skipped)]'.yellow);
    return;
  }

  if (options.adminNotification && !emailData.templateName) {     
    console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, 'Sending notification to admin...'.yellow);

    return self.mailer.sendMail({
      from: emailData.from,
      to: emailData.to,
      subject: emailData.subject,
      html: '<h1>Notification Failed</h1>' +
            '<h3> Job Id: ' + emailData.content.failedJob.id + '</h3>' +
            '<h3> Job Content: </h3>' +
            '<p>' + JSON.stringify(emailData.content) + '</p>',
      
      generateTextFromHTML: true,
    }, function(err, response){
      if (err) {
        console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, ('[Error: ' + err + ']').red);
        return callback(err);
      }

      console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, 'completed successfully'.green, '[response: ' + JSON.stringify(response) + ']');
      callback();
    })    
  }
  
  console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, 'Starting'.blue);
  var template = new EmailTemplate(path.join(self.templatesDir, emailData.templateName)); // , function(err, template){

    if (!template) {
      console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, ('[Error: ' + 'no template' + ']').red);
      return callback(new Error('no template'));
    }

    template.render(emailData, function (err, data) {

      if (err ) {
        console.log('Error', err);
        return callback(err);
      }
      self.mailer.sendMail({
        from: emailData.from,
        to: emailData.to,
        subject: emailData.subject,
        html: data.html,
        text: data.text
      }, function (err, response) {
        if (err) {
          console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, ('[Error: ' + err + ']').red);
          return callback(err);
        }

        console.log('[Notifications.js] '.cyan, 'EmailNotifications: '.magenta, 'completed successfully'.green, '[response: ' + JSON.stringify(response) + ']');
        callback();
      });
    });

};

module.exports = {
  serviceName: 'email',
  notificationManager: new EmailNotificationManager()
};