var EventEmitter  = require("events").EventEmitter;
var jsonTransform = require('json-templater/object');
var Promise       = require('bluebird');
var colors        = require('colors');
var path          = require('path');
var fs            = require('fs');
var _             = require('lodash');
var apn           = require('apn');
var adaptors      = require('../adaptors.js');

function IosNotificationManager(){
  this.configDictionary = {};
  this.isConfigurated = true;
}

IosNotificationManager.prototype.configure = function(bundleId, serviceName, config){
  var configName = serviceName
  this.configDictionary[configName] = {
    config: {}
  };

  var apnConfig = this.configDictionary[configName];

  apnConfig.isConfigurated = true;

  apnConfig.config.production = serviceName === 'ios' ?  true : false;
  apnConfig.config.apnKey     = path.resolve(config.apnKey);
  apnConfig.config.keyId      = config.keyId;
  apnConfig.config.teamId     = config.teamId;

  apnConfig.Provider = new apn.Provider({
    production: apnConfig.config.production,
    token: {
      key: apnConfig.config.apnKey,    // Path to the key p8 file
      keyId: apnConfig.config.keyId,    // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
      teamId: apnConfig.config.teamId  // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
    }
  }).on("feedback", function(devices) {
      if (devices)
        console.log('[Notifications.js] '.cyan, configName.magenta, 'iOSDevNotifications: '.magenta, 'Feedback Success received with data -> '.blue, devices);
    })
    .on("feedback error", function(devices) {
      if (devices)
        console.log('[Notifications.js] '.cyan, configName.magenta, 'iOSDevNotifications: '.magenta, 'Feedback Error received with data -> '.blue, devices);
    });
};

IosNotificationManager.prototype.sendPushNotification = function(user, message, data, options, callback){
  var self = this;
  _.merge(options, {
    sound: 'ping.aiff',
    badge: undefined
  });

  if (!self.isConfigurated) {
    console.log('[Notifications.js]'.cyan, 'iOSDevNotifications: '.magenta, '[Warning: iOSNotification is not configurated (skipped)]'.yellow);
    return callback();
  }

  console.log('[Notifications.js] '.cyan + 'iOSDevNotifications: '.magenta + 'Starting'.blue);

  var notification = new apn.Notification();

  notification.setSound(options.sound);
  if (typeof message === 'object') {
    if (typeof message === 'string') {
      notification.alert = message.message;
    } else {
      notification.alert = message.title + ': ' + message.subtitle;
    }
  } else {
    notification.alert = message;
  }

  if (options.badge) {
    notification.setBadge(options.badge);
  }

  if (adaptors.getUserSilentNotification()(user)) {
    notification.setSound("");
    notification.setContentAvailable(1);
    //notification.priority = 5;
    notification.alert = undefined;
  }

  notification.payload = message || data;
  if(user.devices.iosdev) {
    user.devices.ios = user.devices.iosdev
  }
  _.forEach(user.devices.ios, function(device){
    if(!(device.isActive && device.isActive == true)) {
      return
    }

    var serviceName   = device.dev === false ? 'ios' : 'iosdev'
    var configName    = serviceName
    console.log('[Notifications.js] '.cyan + 'iOSDevNotifications: '.magenta + 'notification sent'.green + ' config name: '+ configName);

    try {
      notification.topic = device.build;
      notification.expiry = Math.floor(Date.now() / 1000) + 3600;

      self.configDictionary[configName].Provider.send(notification, device.deviceId);
    } catch (e) {
      console.log(e.stack);
      return callback(e);
    }
  })

  return callback();
};

module.exports = {
  serviceName: 'ios',
  notificationManager: new IosNotificationManager()
};
