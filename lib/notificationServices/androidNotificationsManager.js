var jsonTransform = require('json-templater/object');
var Promise       = require('bluebird');
var colors        = require('colors');
var path          = require('path');
var fs            = require('fs');
var _             = require('lodash');
var gcm           = require('node-gcm');
var adaptors      = require('../adaptors');

function AndroidNotificationManager(){
  this.configDictionary = {};
  this.isConfigurated = false;
}

AndroidNotificationManager.prototype.configure = function(bundleId, config){
  var configName = bundleId
  this.configDictionary[configName] = {
    sender: new gcm.Sender(config.apiKey)
  };

  this.isConfigurated = true;
};

AndroidNotificationManager.prototype.sendPushNotification = function(user, message, data, options, callback){
  var self = this;

  options = options || {};
  callback = callback || function(){};

  if (!self.isConfigurated) {
    console.log('[Notifications.js]'.cyan + 'AndroidNotifications: '.magenta + '[Warning: AndroidNotificationManager is not configurated (skipped)]'.yellow);
    callback();
    return resolve();
  }

  console.log('[Notifications.js] '.cyan + 'AndroidNotifications: '.magenta + 'Starting'.blue);

  var buildData = adaptors.getDataAdaptor()(message, data);

  var notificationMessage = new gcm.Message({
    collapseKey: options.messageKey || 'notifications-js',
    delayWhileIdle: false,
    data: buildData
  });

  _.forEach(user.devices.android, function(device){
    if(!(device.isActive && device.isActive == true)) {
      return
    }

    var configName    = device.build
    self.configDictionary[configName].sender.send(notificationMessage, device.deviceId, function (err, result) {
      if (err) {
        console.log('[Notifications.js] '.cyan, 'AndroidNotification: '.magenta, ('[Error: ' + err + ']').red);
        return callback(err);
      }

      console.log('[Notifications.js] '.cyan, 'AndroidNotification: '.magenta, 'completed successfully'.green, '[response: ' + JSON.stringify(result) + ']');
      callback();
    });
  })
};

module.exports = {
  serviceName: 'android',
  notificationManager: new AndroidNotificationManager()
};
