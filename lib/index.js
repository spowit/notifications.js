require('colors');

var _ = require('lodash');
var jsonTransform = require('json-templater/object');
var notificationServices = require('./notificationServices');

/* Delivery system, by default none. redis demo is available with delivery_kue */
var delivery;

function Notification(/*config*/) {
  this._notificationsByServiceName = _.indexBy(notificationServices, 'serviceName');
  this.config = {};
}

var adaptors = require('./adaptors');

/*********************************
 *        PUBLIC METHODS
 *********************************/
Notification.prototype.delivery = function() {
  return delivery;
};

Notification.prototype.adaptor = function() {
  return adaptors;
};

Notification.prototype.configure = function (bundleId, config) {
  var self = this;
  self.config = config;

  _.forIn(self.config.services, function (serviceConfig, serviceName) {
    if(serviceName == 'iosdev' || serviceName == 'ios') {
      self._notificationsByServiceName['ios'].notificationManager.configure(bundleId, serviceName, serviceConfig);
    }
    else if (serviceName == 'android') {
      self._notificationsByServiceName[serviceName].notificationManager.configure(bundleId, serviceConfig);
    }
    else {
      self._notificationsByServiceName[serviceName].notificationManager.configure(serviceConfig);
    }

  });

  if (self.config.delivery) {
    delivery = require('./' + self.config.delivery);
  } else {
    delivery = require('./delivery');
  }
  delivery.init();
  delivery.startService(self._notificationsByServiceName);
  delivery.onFailConfig(self.config, self._notificationsByServiceName);
};

Notification.prototype.sendPushNotification = function (users, message, data, options) {
  var self = this;

  if (!_.isArray(users)) {
    users = [users];
  }

  users.forEach(function (user) {

    var userDeviceServices = [];
    user = adaptors.getUserAdaptor()(user);
    if (user.devices) {
      userDeviceServices = _.keys(user.devices);
    }

    availableServices = _.keys(self._notificationsByServiceName);

    userDeviceServices.forEach(function (userDeviceService) {
      if (!_.includes(availableServices, userDeviceService)) {
        if(userDeviceService !== 'iosdev') {
          return console.log('invalid pushNotificationService:', userDeviceService);
        }
      }

      var pushNotificationData = {
        serviceName: userDeviceService,
        user: user,
        message: message,
        data: data,
        options: options
      };

      delivery.pushNotification({data:pushNotificationData}, userDeviceService);

    });
  });
};

Notification.prototype.sendEmail = function (users, emailData, options) {
  var self = this;

  if (!_.isArray(users)) {
    users = [users];
  }

  users.forEach(function (user) {

    var renderedEmailData = jsonTransform(emailData, user);

    if (!renderedEmailData.to) {
      console.log('Error: EmailData field \"to\" is required');
      return;
    }
    if (!renderedEmailData.from) {
      console.log('Error: EmailData field \"from\" is required');
      return;
    }
    if (!renderedEmailData.subject) {
      console.log('Warning: EmailData field \"Subject\" is empty');
    }
    if (!renderedEmailData.templateName) {
      console.log('Error: EmailData field \"templateName\" is required');
      return;
    }
    if (!renderedEmailData.content) {
      console.log('Error: EmailData field \"content\" is required');
      return;
    }

    var emailDataPackage = {
      to: renderedEmailData.to,
      from: renderedEmailData.from,
      subject: renderedEmailData.subject,

      templateName: renderedEmailData.templateName,
      content: renderedEmailData.content
    };

    delivery.sendEmail(emailDataPackage, 'email', user.name);

  })
};


module.exports = Notification;
