var onFinishCB = null;
var onFinish = {
  on: function (string, callback) {
    if (string === 'job complete') {
      onFinishCB = callback;
    }
  },
  run: function (string) {
    var id = 1;
    var result = "ok";
    if (onFinishCB) onFinishCB(id, result);
  }
};

exports.queuedJobs = function() {
  return onFinish;
};

exports.init = function() {
};

var sendEmail;
var sendPush;

exports.startService = function(_notificationsByServiceName) {


  sendEmail = function(job, done) {
    var emailData = job.data;
    var emailServiceManager = _notificationsByServiceName['email'].notificationManager;
    emailServiceManager.sendEmail(emailData, {}, done);
  };

  sendPush = function(job, done) {
    var serviceName     = job.data.serviceName;
    var user            = job.data.user;
    var message         = job.data.message;
    var data            = job.data.data;
    var options         = job.data.options;

    var serviceNameNormalized = serviceName
    if(serviceName === 'iosdev' || serviceName === 'ios') {
      serviceNameNormalized = 'ios'
    }

    var notificationManager = _notificationsByServiceName[serviceNameNormalized].notificationManager;
    notificationManager.sendPushNotification(user, message, data, options, done);
  }
};

exports.onFailConfig = function(config, _notificationsByServiceName) {
};

exports.pushNotification = function(pushNotificationData, userDeviceService, userIdentifier) {
  sendPush(pushNotificationData, function(err) {
    if (err) console.log("sendEmail error", err);
    onFinish.run('complete');
  });
};

exports.sendEmail = function(emailData, userDeviceService, userName) {
  sendEmail({data:emailData}, function(err) {
    if (err) console.log("sendEmail error", err);
    onFinish.run('complete');
  });
};
