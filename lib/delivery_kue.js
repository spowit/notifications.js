var kue = require("kue");

var _kueServerHasStart = false;
var _queuedJobs;

exports.queuedJobs = function() {
  return _queuedJobs;
};

exports.init = function() {

  kue.app.set('title', 'Notifications.js');
  _queuedJobs = kue.createQueue();
  _queuedJobs.on( 'error', function(err) {
    console.log('Kue unhandled exception:', err);
  });

};

exports.startService = function(_notificationsByServiceName) {

  if (_kueServerHasStart) return;

  _kueServerHasStart = true;
  kue.app.listen(3000);

  _queuedJobs.promote(200);
  _queuedJobs.watchStuckJobs(2000);

  _queuedJobs.process('email', 1, function (job, done) {
    var emailData = job.data;
    var emailServiceManager = _notificationsByServiceName['email'].notificationManager;
    var domain = require('domain').create();
    domain.run(function () {
      emailServiceManager.sendEmail(emailData, {}, done);
    });
    domain.on('error', done);
  });

  _queuedJobs.process('pushNotification', 1, function (job, done) {
    var serviceName = job.data.serviceName;
    var user = job.data.user;
    var message = job.data.message;
    var data = job.data.data;
    var options = job.data.options;
    userDeviceServices = _.keys(user.devices);
    availableServices = _.keys(_notificationsByServiceName);
    var domain = require('domain').create();
    var notificationManager = _notificationsByServiceName[serviceName].notificationManager;
    domain.run(function () {
      notificationManager.sendPushNotification(user, message, data, options, done);
    });
    domain.on('error', done);
  });
};

exports.onFailConfig = function(config, _notificationsByServiceName) {
  _queuedJobs.on('job failed', function(id/*, result*/){
    kue.Job.get(id, function(err, failedJob){
      if (err) { console.log('[Notification.js] error while getting failed job.id:', id); return; }

      // send Email to Admin
      console.log('Sending Email to Admin');
      var emailServiceManager = _notificationsByServiceName['email'].notificationManager;

      console.log('self.config:', config);

      if (config.admin.sendFailureEmail) {
        emailServiceManager.sendEmail({
          subject: '[Notification.js] Notification Failed',
          to: config.admin.adminEmail,
          from: config.admin.apiEmail,
          content: {
            failedJob: failedJob
          }
        }, {adminNotification: 1}, function (err) {
          if (err) {
            console.log('Error while sending email to admin:', err);
          } else {
            console.log('Email to Admin sent successfully');
          }
        });
      } else {
        console.log('Email error supressed');
      }
    });
  });
};

exports.pushNotification = function(pushNotificationData, userDeviceService, userIdentifier) {
  var job = _queuedJobs.create('pushNotification', pushNotificationData);
  job.attempts(3).backoff( {type:'exponential'} );
  job.removeOnComplete(true);
  job.on('complete', function(result){
      console.log(userDeviceService, 'pushNotification Job.id:', this.id ,'for', userIdentifier, 'completed with data:', result);
  });
  job.on('failed attempt', function(){
    console.log(userDeviceService, 'pushNotification Job.id:', this.id ,'for', userIdentifier, 'failed attempt');
  });
  job.on('failed', function(){
    console.log(userDeviceService, 'pushNotification Job.id:', this.id ,'for', userIdentifier, 'failed');
  });
  job.save(function(err){
    if(!err) console.log('Enqueued', userDeviceService ,' pushNotification job for user', userIdentifier,' as job.id:', job.id);
  });
};

exports.sendEmail = function(emailData, userDeviceService, userName) {
  var job = _queuedJobs.create('email', emailData);
  job.attempts(3).backoff({type: 'exponential'}); /*.backoff( {delay: 60*60, type:'fixed'} )*/

  job.removeOnComplete(true);

  job.on('complete', function (result) {
    console.log('Email Job.id:', this.id, 'for', userName, 'completed with data:', result);
  });
  job.on('failed attempt', function () {
    console.log(userDeviceService, 'pushNotification Job.id:', this.id, 'for', userName, 'failed attempt');
  });
  job.on('failed', function () {
    console.log("Email Job failed", userName);
  });
  job.save(function (err) {
    if (!err) console.log('Enqueued email job for user', userName, ' as job.id:', job.id);
  });
};