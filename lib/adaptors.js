
/* Transform Push Notification Data */
var dataAdaptor = function(a, b) {
  return {
    message:  a,
    data   :  b
  }
};

/* Transform User */
var userAdaptor = function(user) {
  return user;
};

/* Checks if silent notification must be sent */
var userAdaptorSilentNotification = function(user) {
  return false;
}

exports.getDataAdaptor = function() {
  return dataAdaptor;
};

exports.getUserAdaptor = function() {
  return userAdaptor;
};

exports.getUserSilentNotification = function() {
  return userAdaptorSilentNotification;
};

exports.setDataAdaptor = function(newDataAdaptor) {
  dataAdaptor = newDataAdaptor();
};

exports.setUserAdaptor = function(newUserAdaptor) {
  userAdaptor = newUserAdaptor();
};

exports.setUserSilentNotification = function(newUserAdaptorSilentNotification) {
  userAdaptorSilentNotification = newUserAdaptorSilentNotification();
};


