Notifications.js
================

Multi-notifications API


# Supported Notification Services

1. Email (handlebars + css)
2. Android GCN
3. iOS APN


# Usage

## Global Configuration

```javascript
var notifications = require('notifications.js')

notifications.configure({         
  "services": {
    "email": {
      "smtp": {
        "service": "Mailgun",
        "auth": {
          "user": "my-mailgun@email.com",
          "pass": "my-password"
        }
      },
      "templatesDir": path.join(__dirname, "email_template")
    }
  }
);
```

## Sending Email Notification

```javascript
var contentTemplate = {
  to: '{{email}}',
  from: 'test@test.com',
  subject: 'Testing...',
  
  templateName: 'red_mail',
  
  content: {
    body: 'Hello {{name}}'
  }
}

var users = [{
  name: 'Mario Garces',
  email: 'mario@moblox.io'
}, {
  name: 'Edward Alvarado',
  email: 'edward@@moblox.io'
}]

notifications.sendEmail(users, contentTemplate);
```

#### Notes

1. sendEmail is a deferred function (need of callbacks?)
2. template files must follow this convention:
	* each template must be organised by folders inside `templatesDir` path (defined in Global Configuration) and its name must be consistent with `templateName`
	* stylesheets and html template files must be called as `style.scss` and `html.handlebars`
  
# Roadmap

Add iOS tests
Add Android tests
Fix callback system (No feedback is given if an error happens)